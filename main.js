var storeId = [];

function Product(name, status, price, amount) {
    this.name = name;
    this.status = status;
    this.price = parseInt(price);
    this.amount = parseInt(amount);
    this.Id = this.genarateId(storeId, 10, 100);
    this.signalNewProduct(this.name);
}

Product.prototype.genarateId = function(arrId, min, max) {
    var id = randomInt(min, max);
    if (arrId.indexOf(id) === -1) {
        storeId.push(id);
        return id;
    } else {
        genarateId(arrId, min, max);
    }
};

Product.prototype.signalNewProduct = function(nameProduct) {
    console.log('You have a new product in stock: ' + nameProduct);
};

Product.prototype.sellProduct = function(quantityProduct) {
    this.amount -= quantityProduct;
    console.log('You sold product: ' + this.name + ' amount: ' + quantityProduct + ' items' + ' Still have: ' + this.amount);
};

function randomInt(min, max) {
    var newMin, newMax;

    if (isNaN(min) || isNaN(max)) {
        return;
    }
    (max < min) ? (newMin = max, newMax = min) : (newMin = min, newMax = max);
    var rand = newMin + Math.random() * (newMax + 1 - newMin);
    rand = Math.floor(rand);
    return rand;
}

function Bread(name, status, price, amount, type) {
    Product.apply(this, arguments);
    this.type = type;
}

Bread.prototype = Object.create(Product.prototype);
Bread.prototype.constructor = Bread;

var bread = new Bread('Bread', 'share', 10, 100, 'dark');

Bread.prototype.cuted = function(pieces) {
    console.log('Your bread was cuted to ' + pieces + ' pieces');
};

function Water(name, status, price, amount, volume) {
    Product.apply(this, arguments);
    this.volume = volume;
}

Water.prototype = Object.create(Product.prototype);
Water.prototype.constructor = Water;

Water.prototype.signalNewProduct = function() {
    Product.prototype.signalNewProduct.call(this, this.name);
    console.log('Amount of ' + this.name + ' : ' + this.amount + ' items');
};

var Poltaraska = new Water('Water', 'share', 15, 500, '1.5l');